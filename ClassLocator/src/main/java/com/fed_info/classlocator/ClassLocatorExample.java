package com.fed_info.classlocator;

import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

/**
 *
 * @author anton.v.fedorov
 */
public class ClassLocatorExample {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Please specify class name(s) in arguments");
            System.exit(0);
        }
        for (String className : args) {
            System.out.print(className + ": ");
            try {
                URL location = locate(className);
                System.out.println(location == null ? "-" : location);
            } catch (ClassNotFoundException ex) {
                System.out.println("Not Found!");
            }
        }
    }
    
    private static URL locate(String className) throws ClassNotFoundException {
        Class clazz = Class.forName(className);
        ProtectionDomain pd = clazz.getProtectionDomain();
        CodeSource cs = pd.getCodeSource();
        return (cs == null) ? null : cs.getLocation();
    }
    
}
